package poc.schema.model.millesime;

public class SchemaMillesime {
    public static final String SCHEMA_2018 = "data_2018";
    public static final String SCHEMA_2019 = "data_2019";
    public static final String SCHEMA_NAUTILE = "nautile";
}