package poc.schema.services.service.millesime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import poc.schema.dao.configuration.TenantConnectionProvider;
import poc.schema.dao.repositories.IndividuRepository;
import poc.schema.model.millesime.Individu;

import javax.validation.constraints.NotNull;
import java.util.List;
@Service
public class IndividuServiceImpl implements IndividuService {
    private static Logger logger = LoggerFactory.getLogger(IndividuServiceImpl.class);
    @Autowired
    private IndividuRepository individuRepository;

    @Override
    public List<Individu> findAll(){
        logger.info("findall");
        return individuRepository.findAll();
    }

    @Override
    public Individu getIndividu(long id) {
        logger.info("getIndividu");
        return individuRepository.findById(id).get();
    }
}
